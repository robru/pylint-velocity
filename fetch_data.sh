#!/bin/sh

export HOME=$(readlink --canonicalize "$(dirname "$0")")
export PROJECT="$HOME/$1"
export TARGET="$PROJECT/target"
export PARSE_LINT="$HOME/parse_lint.py"

# Complain if the target does not exist.
[ -d "$TARGET" ] || exit "You must download a source branch into $TARGET for analysis."

. "$HOME/vcs.sh"


# Per-project configuration of pylint.
[ -s "$PROJECT/config.sh" ] || cat >"$PROJECT/config.sh" <<END_OPTS
# List of strings to pass to 'grep' in order to pylint certain subsets of your
# repo. "test" is the default so you can distinguish your tests from your code.
# You probably also want to list all your python modules here, it's up to you.
# Values should be space-separated.
export GREPS="test"

# What ack command do you want to use? This should be something that will produce
# a list of files that you care about on stdout, suitable for piping into xargs.
export ACK="ack -f --python"

# Disable these false positives:
#    import-error: I don't have all the deps installed locally.
#    no-member: Many classes implement __getattr__, and I like it that way.
#    too-few-public-methods: Keep it simple, stupid.
#    too-many-public-methods: More like too-many-tests. Never enough tests.
export LINTER="pylint3 --disable=import-error,no-member,too-few-public-methods,too-many-public-methods"
END_OPTS
. "$PROJECT/config.sh"


# Update to latest revision.
cd "$TARGET"
vcs_revert 2>"$SHH"
vcs_pull 2>"$SHH"


# Delete potentially-corrupted data files if we get killed halfway through.
trap cleanup INT


# Scan all missing revisions, newest first.
for ID in $(vcs_list | tac); do
    REV=$(echo "$ID" | cut --fields 2 --delimiter .)

    cleanup() {
        rm -vrf "$PROJECT/$ID."*
        exit 0
    }

    MAIN_LINT="$PROJECT/$ID.total.lint"
    BLAME_LOG="$PROJECT/$ID.blame.log"

    MISSING=""
    for GREP in $GREPS; do
        [ -s "$PROJECT/$ID.$GREP.lint" ] || MISSING="x$MISSING"
    done
    for FILE in $MAIN_LINT $BLAME_LOG; do
        [ -s "$FILE" ] || MISSING="x$MISSING"
    done

    if [ -z "$MISSING" ]; then
        # All files created already; don't waste time checking out rev.
        continue
    fi

    echo; vcs_get "$REV" 2>"$SHH"
    echo "Populating data files for revision $REV..."
    for GREP in $GREPS; do
        FILENAME="$PROJECT/$ID.$GREP.lint"
        [ -s "$FILENAME" ] || $ACK | grep "$GREP" | xargs $LINTER 2>"$SHH" | "$PARSE_LINT" | sort >"$FILENAME"
    done
    [ -s "$MAIN_LINT" ] || $ACK | xargs $LINTER 2>"$SHH" | "$PARSE_LINT" | sort >"$MAIN_LINT"
    [ -s "$BLAME_LOG" ] || vcs_blame "$REV" >"$BLAME_LOG"
    echo; wc --bytes "$PROJECT/$ID."*
done
