#!/usr/bin/env python3

"""Parse the output of pylint and store it in a graphable format."""

import sys

from contextlib import suppress


WATCH = {
    'Report\n',
    'Statistics',
    'Raw',
    'Messages\n',
    'Messages',
    'Global',
    'Duplication\n'
}
IGNORE = {'External', '%'}


watch = False
for line in sys.stdin:
    token = line.partition(' ')[0]
    if token in WATCH:
        watch = True
    if token in IGNORE:
        watch = False
    if watch:
        if 'rated at' in line:
            print('rating:', line.split('/')[0].split()[-1])
        if 'statements analysed.' in line:
            print('statements:', line.split()[0])
        parts = line.split('|')
        if len(parts) >= 3:
            series = parts[1].strip()
            y_value = parts[2].strip()
            with suppress(ValueError):
                print('{}: {}'.format(series, float(y_value)))

print('(done)')
