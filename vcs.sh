#!/bin/sh

SHH="/dev/null"
ENUMERATE="nl --number-format rz --number-separator ."

# If you have binary files checked in to bzr, the blame data will be reported ok,
# but awk will totally puke when you try to extract the email address from the line.
# This strips out unsafe characters so that awk can perform as expected.
STRIP="tr --delete '\0-\11'"

loudly() {
    echo "$@"
    "$@"
}

vcs_pull() {
    [ -d .bzr ] && loudly bzr pull
    [ -d .git ] && loudly git pull
}

vcs_revert() {
    [ -d .bzr ] && loudly bzr revert
    [ -d .git ] && loudly git checkout master
}

vcs_list() {
    [ -d .bzr ] && bzr log --line --forward | $ENUMERATE | cut --fields 1 --delimiter :
    [ -d .git ] && git log --oneline --first-parent --date-order --reverse \
        | $ENUMERATE | cut --fields 1 --delimiter ' '
}

vcs_get() {
    [ -d .bzr ] && loudly bzr revert --revision="$1" && loudly bzr clean-tree --ignored --detritus --force --quiet
    [ -d .git ] && loudly git checkout "$1" && loudly git clean --force -d
}

vcs_blame() {
    [ -d .bzr ] && bzr ls --versioned --recursive --kind file --null \
        | xargs --null --max-args 1 bzr blame --long --all --revision "$1" | $STRIP \
        | awk '{ print $2 }' | sort | uniq --count | sort --numeric-sort --reverse \
        | awk '{ print $1,"author",$2 }'
    [ -d .git ] && git ls-tree -r -z --name-only HEAD \
        | xargs --null --max-args 1 git blame --line-porcelain HEAD 2>$SHH \
        | grep  "^author " | sort | uniq --count | sort  --numeric-sort --reverse
}
