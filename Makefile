default:
	for DIR in */; do make fetch-$$DIR; make graph-$$DIR; done

fetch-%:
	./fetch_data.sh $*

graph-%:
	rm -rf $*/*.svg
	./graph.py $*
	xdg-open $*/statements.svg

example:
	rm -rf meta/
	mkdir meta
	echo 'export GREPS="graph"' > meta/config.sh
	echo 'export ACK="ack -f --python"' >> meta/config.sh
	echo 'export LINTER="pylint3"' >> meta/config.sh
	git clone https://gitlab.com/robru/pylint-velocity.git meta/target
	make fetch-meta
	make graph-meta

profile-%:
	python3 -m cProfile -s cumulative ./graph.py $* | less

check-%:
	@echo "Will now regenerate 10 random data files and check them for consistency."
	mkdir -p $*/old
	cd $*; ls *.lint *.log | shuf | head | xargs -I{} mv '{}' old/
	make fetch-$*
	cd $*/old; for FILE in *; do diff -Naur $$FILE ../$$FILE; done
	rm -rf $*/old
	@echo "Done. If you did not see any diff output above, that means your files are consistent."

check:
	find -name '*.py' | xargs python3 -m pyflakes
	find -name '*.py' | xargs python3 -m pep8
	find -name '*.py' | xargs python3 -m pylint --rcfile=.pylintrc
